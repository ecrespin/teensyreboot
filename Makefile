OS ?= LINUX
#OS ?= WINDOWS
#OS ?= MACOSX
#OS ?= BSD

ifeq ($(OS), LINUX)  # also works on FreeBSD
CC ?= gcc
CFLAGS ?= -O2 -Wall
teensy_reboot: teensy_reboot.c
	$(CC) $(CFLAGS) -s -DUSE_LIBUSB -o teensy_reboot teensy_reboot.c -lusb


else ifeq ($(OS), WINDOWS)
CC = i586-mingw32msvc-gcc
CFLAGS ?= -O2 -Wall
teensy_reboot.exe: teensy_reboot.c
	$(CC) $(CFLAGS) -s -DUSE_WIN32 -o teensy_reboot.exe teensy_reboot.c -lhid -lsetupapi


else ifeq ($(OS), MACOSX)
CC ?= gcc
SDK ?= /Developer/SDKs/MacOSX10.5.sdk
CFLAGS ?= -O2 -Wall
teensy_reboot: teensy_reboot.c
	$(CC) $(CFLAGS) -DUSE_APPLE_IOKIT -isysroot $(SDK) -o teensy_reboot teensy_reboot.c -Wl,-syslibroot,$(SDK) -framework IOKit -framework CoreFoundation


else ifeq ($(OS), BSD)  # works on NetBSD and OpenBSD
CC ?= gcc
CFLAGS ?= -O2 -Wall
teensy_reboot: teensy_reboot.c
	$(CC) $(CFLAGS) -s -DUSE_UHID -o teensy_reboot teensy_reboot.c


endif


clean:
	rm -f teensy_reboot teensy_reboot.exe
