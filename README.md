#Teensy Reboot - Command Line Version#

### Reboot without unplug & plug ###

If your Teensy ends up in reset mode and you need to restart from software (without unplugging and re-plugging USB), download and make this repository and execute with 


```
#!python

sudo ./teensy_reboot -mmcu=mk20dx256
```


The Teensy Reboot is based on Paul's  teensy\_loader\_cli. 

http://www.pjrc.com/teensy/loader_cli.html

![Teensy 3.1](http://www.pjrc.com/teensy/teensy31_front_small_green.jpg)